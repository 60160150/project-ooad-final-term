"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _vueRouter = _interopRequireDefault(require("vue-router"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

_vue["default"].use(_vueRouter["default"]);

var routes = [{
  path: '/mainAdmin',
  name: 'mainAdmin',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/admin/main.vue'));
    });
  }
}, {
  path: '/manageEducate',
  name: 'manageEducate',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/admin/Manage/declareEducate.vue'));
    });
  }
}, {
  path: '/manageDocs',
  name: 'manageDocs',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/admin/manageDocs.vue'));
    });
  }
}, {
  path: '/manageCoop',
  name: 'manageCoop',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/admin/Manage/declareCoop.vue'));
    });
  }
}, {
  path: '/listCoop/:_id',
  name: 'listCoop',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/admin/Manage/list/coop.vue'));
    });
  }
}, {
  path: '/listEducate/:_id',
  name: 'listEducate',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/admin/Manage/list/educate.vue'));
    });
  }
}, {
  path: '/mainStudent/',
  name: 'mainStudent',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/student/main.vue'));
    });
  }
}, {
  path: '/showDocs',
  name: 'showDocs',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/noLogin/showDocs.vue'));
    });
  }
}, {
  path: '/main',
  name: 'main',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/noLogin/main.vue'));
    });
  }
}, {
  path: '/',
  name: '/',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/noLogin/main.vue'));
    });
  }
}, {
  path: '/showListEducate',
  name: 'showListEducate',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/noLogin/showListEducate.vue'));
    });
  }
}, {
  path: '/login',
  name: 'login',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/noLogin/login.vue'));
    });
  }
}, {
  path: '/showEducate',
  name: 'showEducate',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/noLogin/showEducate.vue'));
    });
  }
}, {
  path: '/showCooperative',
  name: 'showCooperative',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/noLogin/showCooperative.vue'));
    });
  }
}, {
  path: '/',
  name: '',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/noLogin/main.vue'));
    });
  }
}, {
  path: '/students',
  name: 'students',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/admin/students/students.vue'));
    });
  }
}, {
  path: '/companies',
  name: 'companies',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/admin/companies/company.vue'));
    });
  }
}, {
  path: '/companies/student',
  name: 'student',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/admin/companies/student.vue'));
    });
  }
}, {
  path: '/mainStusent/interviewEligibility',
  name: 'interviewEligibility',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/student/listStudent.vue'));
    });
  }
}, {
  path: '/checkCourse',
  name: 'checkCourse',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/student/checkCourse.vue'));
    });
  }
}, {
  path: '/checkEducateHours',
  name: 'checkEducateHours',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/student/checkEducateHours.vue'));
    });
  }
}, {
  path: '/regisEducater',
  name: 'regisEducater',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/student/regisEducater.vue'));
    });
  }
}, {
  path: '/regisCooperative',
  name: 'regisCooperative',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/student/regisCooperative.vue'));
    });
  }
}, {
  path: '/showEducateListStudent',
  name: 'showEducateListStudent',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('../views/student/showEducateListStudent.vue'));
    });
  }
}];
var router = new _vueRouter["default"]({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes
});
var _default = router;
exports["default"] = _default;