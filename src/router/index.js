import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/mainAdmin',
    name: 'mainAdmin',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/admin/main.vue')
  },
  {
    path: '/manageEducate',
    name: 'manageEducate',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/admin/Manage/declareEducate.vue'
      )
  },
  {
    path: '/manageDocs',
    name: 'manageDocs',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/admin/manageDocs.vue')
  },
  {
    path: '/manageCoop',
    name: 'manageCoop',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/admin/Manage/declareCoop.vue'
      )
  },
  {
    path: '/listCoop/:_id',
    name: 'listCoop',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/admin/Manage/list/coop.vue'
      )
  },
  {
    path: '/listEducate/:_id',
    name: 'listEducate',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/admin/Manage/list/educate.vue'
      )
  },
  {
    path: '/mainStudent/',
    name: 'mainStudent',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/student/main.vue')
  },
  {
    path: '/showDocs',
    name: 'showDocs',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/noLogin/showDocs.vue')
  },
  {
    path: '/main',
    name: 'main',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/noLogin/main.vue')
  },
  {
    path: '/',
    name: '/',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/noLogin/main.vue')
  },
  {
    path: '/showListEducate',
    name: 'showListEducate',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/noLogin/showListEducate.vue'
      )
  },
  {
    path: '/login',
    name: 'login',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/noLogin/login.vue')
  },
  {
    path: '/showEducate',
    name: 'showEducate',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/noLogin/showEducate.vue')
  },
  {
    path: '/showCooperative',
    name: 'showCooperative',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/noLogin/showCooperative.vue'
      )
  },
  {
    path: '/',
    name: '',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/noLogin/main.vue')
  },
  {
    path: '/students',
    name: 'students',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/admin/students/students.vue'
      )
  },
  {
    path: '/companies',
    name: 'companies',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/admin/companies/company.vue'
      )
  },
  {
    path: '/companies/student',
    name: 'student',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/admin/companies/student.vue'
      )
  },
  {
    path: '/mainStusent/interviewEligibility',
    name: 'interviewEligibility',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/student/listStudent.vue')
  },
  {
    path: '/checkCourse',
    name: 'checkCourse',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/student/checkCourse.vue')
  },
  {
    path: '/checkEducateHours',
    name: 'checkEducateHours',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/student/checkEducateHours.vue')
  },
  {
    path: '/regisEducater',
    name: 'regisEducater',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/student/regisEducater.vue'
      )
  },
  {
    path: '/regisCooperative',
    name: 'regisCooperative',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/student/regisCooperative.vue'
      )
  },
  {
    path: '/showEducateListStudent',
    name: 'showEducateListStudent',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/student/showEducateListStudent.vue'
      )
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
